package com.flightbook.tests;

import java.util.Map;

import org.testng.annotations.Test;

import com.flightbook.pages.HomePage;
import com.flightbook.pages.SearchPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class FlightRoundTripTestXML extends WebDriverTestCase {
	
	@QAFDataProvider(key="location.places")
	@Test
	public void testRoundTrip(Map<String, String> data)
	{
		 HomePage homepage = new HomePage();
		homepage.launchPage();
		homepage.bookFlight(data.get("origin"),data.get("destination"));
		
		SearchPage searchpage = new SearchPage();
		searchpage.waitForPageToLoad();
		searchpage.displaySearchResult();
		searchpage.validateFewResult();
	}

}
