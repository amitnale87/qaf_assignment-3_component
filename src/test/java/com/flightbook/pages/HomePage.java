package com.flightbook.pages;

import java.util.List;
import java.util.Map;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "link.flightsLink.homepage")
	private QAFWebElement flightsLink;
	@FindBy(locator = "txt.flightOrigin.homepage")
	private QAFWebElement flyingFromTextField;
	@FindBy(locator = "txt.flightDestination.homepage")
	private QAFWebElement flyingToTextField;
	@FindBy(locator = "link.fromCity.homepage")
	private QAFWebElement firstFromCity;
	@FindBy(locator = "link.toCity.homepage")
	private QAFWebElement firstToCity;
	@FindBy(locator = "btn.search.homepage")
	private QAFWebElement searchButton;
	@FindBy(locator = "list.depatureDate.homepage")
	private List<QAFWebElement> activeDeparttureDateList;
	@FindBy(locator = "list.returningDate.homepage")
	private List<QAFWebElement> activeReturnDateList;
	@FindBy(locator = "txt.flightDepating.homepage")
	private QAFWebElement departDateTextField;
	@FindBy(locator = "txt.flightReturning.homepage")
	private QAFWebElement ruturnDateTextField;

	
	public QAFWebElement getFlightsLink() {
		return flightsLink;
	}

	public QAFWebElement getFlyingFromTextField() {
		return flyingFromTextField;
	}

	public QAFWebElement getFlyingToTextField() {
		return flyingToTextField;
	}

	public QAFWebElement getFirstFromCity() {
		return firstFromCity;
	}

	public QAFWebElement getFirstToCity() {
		return firstToCity;
	}

	public QAFWebElement getSearchButton() {
		return searchButton;
	}

	public List<QAFWebElement> getActiveDeparttureDateList() {
		return activeDeparttureDateList;
	}

	public List<QAFWebElement> getActiveReturnDateList() {
		return activeReturnDateList;
	}

	public QAFWebElement getDepartDateTextField() {
		return departDateTextField;
	}

	public QAFWebElement getRuturnDateTextField() {
		return ruturnDateTextField;
	}
	
	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
		driver.get("/");
		driver.manage().window().maximize();
	}
	
	public void launchPage() {
		/*Launch an application*/
		openPage(null, null);
	}
	
	@QAFDataProvider(dataFile="resources/data/flightSourceNDestinationData.csv")
	public void bookFlight(String origin, String dest)
	{
		flightsLink.click();
		
		/*Select From City*/
		flyingFromTextField.click();
		//flyingFromTextField.sendKeys(data.get("Origin"));
		
		flyingFromTextField.sendKeys(origin);
		firstFromCity.waitForVisible(3000);
		firstFromCity.click();
		
		/*Select To City*/
		flyingToTextField.click();
		//flyingToTextField.sendKeys(data.get("Destination"));
		flyingToTextField.sendKeys(dest);
		firstToCity.waitForVisible(3000);
		firstToCity.click();
		
		
		/*Select Departure Date*/
		departDateTextField.click();
		activeDeparttureDateList.get(1).click();
		
		/*Select Return Date*/
		ruturnDateTextField.click();
		activeReturnDateList.get(2).click();
		
		searchButton.click();
		
		
	}
	

}
