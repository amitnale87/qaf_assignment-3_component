package com.flightbook.pages;

import java.util.List;

import org.hamcrest.Matchers;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;



import com.flightbook.components.*;

public class SearchPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "searchResultList")
	private List<ComponentPage> searchResultList;
	
	@FindBy(locator = "lowestPrice")
	private QAFWebElement lowestPrice;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public List<ComponentPage> getSearchResultList() {
		return searchResultList;
	}

	public QAFWebElement getLowestPrice() {
		return lowestPrice;
	}
	
	/*Display Searched result*/
	public void displaySearchResult()
	{
		
		Reporter.log("The Count of avialable flight is "+searchResultList.size());
		for (int i=0; i<searchResultList.size();i++)
		{
			searchResultList.get(i).getTxtFlightName().waitForVisible();
			Reporter.log("Flight Name: "+searchResultList.get(i).getTxtFlightName().getText());
			
			searchResultList.get(i).getTxtArrivalTime().waitForVisible(3000);
			Reporter.log("Flight Arrival Time: "+searchResultList.get(i).getTxtArrivalTime().getText());
			
			searchResultList.get(i).getTxtDepartTime().waitForVisible(3000);
			Reporter.log("Flight Departure Time: "+searchResultList.get(i).getTxtDepartTime().getText());
			
			searchResultList.get(i).getTxtFlightPrice().waitForVisible();
			Reporter.log("Flight Fare Amount: "+searchResultList.get(i).getTxtFlightPrice().getText());
			
		}
		Reporter.log("\n");
	}
	
	/*validate searched flight contains proper from-to cities*/
	public void validateFewResult()
	{
		for (int i=0; i<5;i++)
		{
			//Validator.assertThat(searchResultList.get(i).getTxtSourceDestination().getText(), Matchers.containsString("PNQ - BOM"));
			/*Validator.assertThat(searchResultList.get(i).getTxtSourceDestination().getText(),
					Matchers.containsString("Departure airport:\n" +"PNQ -\n" +"Arrival airport:\n" + "BOM"));*/
			
						
		}
		/*Validate search page title is as expected */
		Validator.assertThat(driver.getTitle(), Matchers.containsString("PNQ to BOM"));
		
	}

}
