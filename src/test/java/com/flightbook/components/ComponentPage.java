package com.flightbook.components;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ComponentPage extends QAFWebComponent{

	public ComponentPage(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

	@FindBy(locator = "txt.flightName")
	private QAFWebElement txtFlightName;
	@FindBy(locator = "txt.sourceDestination")
	private QAFWebElement txtSourceDestination;
	@FindBy(locator = "txt.departTime")
	private QAFWebElement txtDepartTime;
	@FindBy(locator = "txt.arrivalTime")
	private QAFWebElement txtArrivalTime;
	@FindBy(locator = "txt.totalTime")
	private QAFWebElement txtTotalTime;
	@FindBy(locator = "txt.flightPrice")
	private QAFWebElement txtFlightPrice;
	
	

	

	public QAFWebElement getTxtFlightName() {
		return txtFlightName;
	}

	public QAFWebElement getTxtSourceDestination() {
		return txtSourceDestination;
	}

	public QAFWebElement getTxtDepartTime() {
		return txtDepartTime;
	}

	public QAFWebElement getTxtArrivalTime() {
		return txtArrivalTime;
	}

	public QAFWebElement getTxtTotalTime() {
		return txtTotalTime;
	}

	public QAFWebElement getTxtFlightPrice() {
		return txtFlightPrice;
	}

}
